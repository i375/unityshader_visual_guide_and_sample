# Visual guide and sample of Unity3D shader parameters and data paths

[![Visual guide](visual_guide.png)](visual_guide.png)

# References

[ShaderLab syntax: Properties](https://docs.unity3d.com/455/Documentation/Manual/SL-Properties.html) 

[Accessing shader properties in Cg/HLSL](https://docs.unity3d.com/Manual/SL-PropertiesInPrograms.html)

[Built-in shader variables](https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html)

# Additional resources

[CG/HLSL Value semantics](https://msdn.microsoft.com/en-us/library/bb509647(v=vs.85).aspx)

[CG/HLSL Intrinsic/BuiltIn functions](https://msdn.microsoft.com/en-us/library/ff471376(v=vs.85).aspx)

[Graphics Concepts (DirectX)](http://www.directxtutorial.com/Lesson.aspx?lessonid=11-4-1)

[Programming Guide for Direct3D 11](https://msdn.microsoft.com/en-us/library/windows/desktop/ff476345.aspx)