﻿Shader "Unlit/LightlessShader"
{
	Properties
	{
    //  Param name      Display name      Type           Default value   Options
    //   ↓               ↓                 ↓              ↓               ↓ 
        _2DTexture      ("Texture",       2D)          = "white"         { }
        _SomeColor      ("Some color",    Color)       = (1,1,1,1)
        _SomeRange      ("Some range",    Range(0,1))  = 0.0
        _ScaleScalar    ("Scale scalar",  Float)       = 0.0
        _SomeVector     ("Some verctor",  Vector)      = (0,0,0,0)
        _SomeCubemap    ("Some cube map", CUBE)        = ""              { }
	}
	SubShader
	{ 
        Tags { "RenderType"="Opaque" }

        // For transparent rendering use following code (commend line above and uncomment 3 lines below).
        //
        // Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
        // ZWrite Off
        // Blend SrcAlpha OneMinusSrcAlpha

		LOD 100
		Pass
		{
			CGPROGRAM
			#pragma vertex vert // Computes vertex positions
			#pragma fragment frag // Computes final pixel colors
			
			#include "UnityCG.cginc"

			sampler2D   _2DTexture;
            float4      _SomeColor;
            float       _SomeRange;
            float       _ScaleScalar;
            float4      _SomeVector;
            samplerCUBE _MyCubemap;

            // TRANSFORM_TEX macro is used for handling
            // texture tiling and offseting.
            // {texture name}_ST has to be defined for
            // TRANFORM_TEX macro to work.
            float4      _2DTexture_ST; 

			struct appdata
			{
				float4 vertex : POSITION; // field for reading vertex positions
                float4 color: COLOR; // field for reading vertex colors
				float2 uv : TEXCOORD0; // field for reading texture UV coordiantes
			};

			struct v2f
			{
				float2 uv : TEXCOORD0; // passing uv cooridnates to fragment shader
                float4 color: COLOR; // passing color to fragment shader
				float4 vertex : SV_POSITION; // passing tranformed vertex coordiantes to fragment shader
			};

			v2f vert (appdata v)
			{
				v2f o;

                v.vertex = v.vertex + _SomeVector;

				o.vertex = UnityObjectToClipPos(v.vertex);

                //         TRANSFORM_TEX looks for _2DTexture_ST
				o.uv     = TRANSFORM_TEX(v.uv, _2DTexture); 
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{				
                // Sampling texture (getting color out of texture)
				fixed4 col = tex2D(_2DTexture, i.uv); 

                // Mixing color from texture with color 
                // passed in through _SomeColor. 
                //
                // _SomeRange determines which color 
                // will have more weight.
                col = lerp(col, _SomeColor, _SomeRange);

				return col;
			}
			ENDCG
		}
	}
}
